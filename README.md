# Nonnegative Simultaneous Sparse Coding

Nonnegative Simultaneous Sparse Coding consists in the following problem:
given $`X \in \mathbb{R}^{m \times n}`$, $`D \in \mathbb{R}^{m \times s}`$, and  $`r \in \mathbb{N}`$,
find $`H \in \mathbb{R}^{s \times n}`$ by solving
$`\min_{H \geq 0} \|X-DH\|_1^2 \text{ tel que } |\{ i | H(i,:) \neq 0 \}| \leq r`$.

## Test

TODO

## Heuristic

TODO
