function [sK] = homotopie(A,b,r)
    [~,n] = size(A);
    
    lambda = [];
    k      = 0;
    class=Inf(n,1);
    AtA = A'*A;
    Atb = A'*b;
    tol = 1e-12;
    nZ  = zeros(n,1);
    
    K   = false(n,1); 
    a   = nZ;
    sK=[];
    sa=[];
    
    [~,j]   = max(Atb);
    pLambda = Atb(j); %lambda potentiel
    ind     = j;
  
    flag    = 'join';

    while pLambda>tol
        %On ajoute ce lambda potentiel
        if strcmp(flag,'join')
            K(ind) = true;
            class(ind)=k+1;
        end
        if strcmp(flag,'remove')
            K(ind) = false;
        end
        lambda = [lambda ; pLambda];
        k      = k+1;
        
        sK=[sK,K];
        
        %Calculs de aK, bK, cK, dK
        a    = nZ;
        a(K) = AtA(K,K)\Atb(K);
        b    = nZ;
        b(K) = AtA(K,K)\ones(sum(K),1);
        c    = nZ;
        c(~K) = AtA(~K,K)*a(K)-Atb(~K);
        d    = nZ;
        d(~K) = AtA(~K,K)*b(K)-ones(sum(~K),1);
        sa=[sa,a];      
        Q1 = b < -tol;
        Q2 = d < -tol;
        
        v1        = -Inf(n,1);
        v1(K&Q1)    = a(K&Q1)./b(K&Q1);
        [max1,k1] = max(v1);
        
        v2        = -Inf(n,1);
        v2(~K&Q2)    = c(~K&Q2)./d(~K&Q2);
        [max2,k2] = max(v2);
        
        if max1 > max2 || sum(K)==r
            pLambda = max1;
            ind     = k1;
            flag    = 'remove';
        else
            pLambda = max2;
            ind     = k2;
            flag    = 'join';
        end
    end
    x = a;
    for i=1:n
        if(x(i)<1e-10)
            K(i)=false;
        end
        
    end 
end