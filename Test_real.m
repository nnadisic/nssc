clear variables; clc;
addpath('./util');
addpath('./heuristic_nssc');
addpath('./heuristic_nssc/prox-l1oo');
addpath('./hierclust2nmf_v2');
addpath 'C:\Users\alexi\Matlab\ancien code'
addpath 'Acc_MU_HALS_PG'
addpath 'web'
addpath 'demo_fgnsr_v2'
%addpath ('C:\Users\alexi\Matlab\demo_fgnsr_v2\demo_fgnsr\fgnsr-0.1\matlab\private')

%% Parameters
r = 8;    % number of columns of W, to select from the dictionary
nc=100; % number of cluster 
maxiter=500; % for the NNLS
tl=5*60; % limit time 
option=1;
maxiterFG = 2000; 
maxitNNLS = 500; % TODO check value

%% Data 
% load("data/Urban.mat");
load("data/SanDiego.mat");


A=A';

% [m, n] = size(A);

%% Préprocessing 

% load saveC.mat;

[IDX, C, J, ~] = hierclust2nmf(A,nc);
X=C;
for i=1:nc
  X(:,i)= X(:,i).*sqrt(J(i));
end
 save('saveC.mat','C','X','IDX');
for j=1:nc
    C(:,j)=C(:,j)./norm(C(:,j),2);

end
%% FGNSR


[H, K] = fgnsr(X, r, ones(size(C,2),1), 'maxiter', maxiterFG, 'verbose', false, 'proj', 'parproj', 'col_weights', ones(size(C,2),1) ); 
% By default, fgnsr returns the r largest diagonal entries of X
% In the paper paper, we use: 
K = FastSepNMF(H',r); 
H = nnlsHALSupdt(A,C(:,K),[],maxitNNLS);
EF = 100*norm(A - C(:,K)*H,'fro')/norm(A,'fro');

[J,myH,results] = exactM(X,C,r,tl);
H = nnlsHALSupdt(A,C(:,J),[],maxitNNLS);
ESE=norm(A-C(:,J)*H ,'fro')/norm(A,'fro')*100;


%% Display results  
fprintf('-----------------------------------\n')
fprintf('Relative error with FGNSR-100   = %2.2f %%\n', EF)
fprintf('Relative error with NNS-OMP  = %2.2f %%\n', ESE)
fprintf('-----------------------------------\n')

