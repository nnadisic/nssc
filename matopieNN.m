function [J,H,sK]=matopieNN(X,D,r)
% initialisation
[~, n] = size(X);
[m, s] = size(D);
H=sparse(s,n);
J=sparse(1,r);
% algorithme faisant appel � l'homotopie pour obtenir les lamdas de chaque
% lignes pour chaque colonnes lorsqu'elles rentrent dans le set K  
% pour faire bouger la ligne il faut que ce soit la moyenne des ak bk ck dk
% 

 lambda = [];
    k      = 0;
    
    AtA = sparse(D'*D);
    AtB = sparse(D'*X);
    tol = 1e-12;
    nZ  = sparse(s,1);
    nnZ=sparse(s,n);
    
    K   = false(s,n); 
    sK=[];
    a   = sparse(s,n);
    
    [v,i]   = max(mean(AtB,2));
    pLambda = v; %lambda potentiel
    ind     = i;
    flag    = 'join';
    tic;
    t=0;
    while pLambda>tol && t<60*5
        %display(k);
        %On ajoute ce lambda potentiel
        if strcmp(flag,'join')
            K(ind,:) = true;
            
        end
        if strcmp(flag,'remove')
            K(ind,:) = false;
        end
        sK=[sK,K(:,1)];
        lambda = [lambda ; pLambda];
        k      = k+1;
        
        %Calculs de aK, bK, cK, dK
        a    = nnZ;
        b    = nnZ;
        c    = nnZ;
        d    = nnZ;
        
         
            a(K(:,1),:) = AtA(K(:,1),K(:,1))\AtB(K(:,1),:);
        
            b(K(:,1),:) = AtA(K(:,1),K(:,1))\ones(sum(K(:,1)),n);
      
            c(~K(:,1),:) = AtA(~K(:,1),K(:,1))*a(K(:,1),:)-AtB(~K(:,1),:);
       
            d(~K(:,1),:) = AtA(~K(:,1),K(:,1))*b(K(:,1),:)-ones(sum(~K(:,1)),n);
            
        Q1 = b < -tol;
        Q2 = d < -tol;
        
        v1        = -Inf(s,n);
        v1(K&Q1)    = a(K&Q1)./b(K&Q1);
        [max1,k1]   = max(mean(v1,2));
        
        v2        = -Inf(s,n);
        v2(~K&Q2)    = c(~K&Q2)./d(~K&Q2);
        [max2,k2]   = max(mean(v2,2));
        
        if max1 > max2 
            pLambda = max1;
            ind     = k1;
            flag    = 'remove';
        else
            pLambda = max2;
            ind     = k2;
            flag    = 'join';
        end
        t=t+toc;
    end
    
    % selection des r
%     class=sum(sK,2);
%     [~,I]=sort(class,'descend');
%     perm=1:s;
%     in=perm(I);
%    
%     
% %     


    
   
    
    
    pJ=find(K(:,1));
    if length(pJ)>r
        som=sum(sK(K(:,1),:),2);
        [~,I]=sort(som,'descend');
        perm=1:s;
        ind=perm(I);
        J=pJ(ind(1:r));
    else
        J=pJ;
    end    
       for j=1:n 
         H(J,j) = lsqnonneg(D(:,J),X(:,j));
       end
end 