clear variables; clc;
addpath('./util');
addpath('./heuristic_nssc');
addpath('./heuristic_nssc/prox-l1oo');

% Fixed random seed
%rng(123)

%% Parameters
s = 100;   % number of columns of the dictionary D, to extract from USGS
r = 4;    % number of columns of W, to select from the dictionary
n = 200;  % number of data points (numer of columns of input X)
itt=100;% itt�ration par niveau de bruit 
nb=1; % nombre de niveau de bruit entre 0 et 50% 
nl = linspace(0,1,nb); % noise level to add to generated data
choix=3; % 1 test 1 2 test 2
%% Data generation
% Select a subset of the USGS dataset to build a dictionary D
load("data/USGS_Library.mat");
[m, nbcol_usgs] = size(datalib);
%% test1
if choix==1
%% Initialisation 
succesG=zeros(nb,1); % greedy
succesE=zeros(nb,1); % exacte 
succesE2=zeros(nb,1); % exacte 
trouve=zeros(nb,1);
taille=zeros(nb,1);
%D = datalib(:,randperm(nbcol_usgs-5, s)+5);
illcondA = 0;
for i=1:nb
    
    disp(i);
    for j=1:itt
        % Build small dictionary by taking s random column from USGS and avoid the
         % first 5 elements (werid spectra)
       D = datalib(:,randperm(nbcol_usgs-5, s)+5);
        if illcondA == 1
        [u,~,v] = svd(D);
        d = diag( logspace(-6,0,s) );
         D = u(:,1:s)*d(1:s,1:s)*v(:,1:s)';
        end
        % normalisation des colonnes de D 
        for j=1:s
            D(:,j)=D(:,j)./norm(D(:,j));
        end 
     % Generate synthetic data
     realH = zeros(s, n);
     realJ = randperm(s, r);
     realH(realJ,:) = rand(r, n);
     X = D*realH;

    % Add noise
     noise = randn(m,n);  
     Xn = X + nl(i) * noise/norm(noise,'fro') * norm(X,'fro');

    %% Run algorithms

    % TODO NN Fix this method
    % Run projected gradient with l_{1,inf} projection
    % options = [];
    % % First find one good lambda automatically (may be too slow on big data)
    % [~, lambda] = findlambda_fpgm_nssc(Xn, D, r, options);
    % %lambda = 0; % You can also predefine lambda yourself
    % % Then run the algorithm with the lambda found
    % [fpgH,~,~] = nnls_FPGM_nssc(Xn, D, lambda, options);
    % fpgJ = nonzerorows(fpgH);

    % Run NNS_greedy (greedy algorithm)
%     greedyH = NNS_greedy(D,Xn,r);
%     greedyJ = nonzerorows(greedyH);

    % Run our algorithm
    % TODO Alexandra
%         tic;
%         [myJ, myW, myH] = myalgo(Xn, D, r);
%         te=toc;
        tic;
        [myH2,myJ2]=matopie3(Xn,D,r);
        te=toc;
%     fprintf('Le temps d execution methode exacte %d \n',te);


    %% Plots and comparison
     %if sum(realJ==greedyJ)==length(realJ)
%      if isequal(sort(realJ), sort(greedyJ))
%         succesG(i)=succesG(i)+1;
%      end 
%      %if sum(realJ==myJ)==length(realJ)
%      if isequal(sort(realJ), sort(myJ))
%         succesE(i)=succesE(i)+1;   
%      end 
%       if isequal(sort(realJ), sort(myJ2))
%         succesE2(i)=succesE2(i)+1;   
%   	 end  
        cond=true;
        for v=1:length(realJ)
            if(~length(find(myJ2==realJ(v))))
                cond=false;
                break;
            end 
                
        end 
        if cond==true
            trouve(i)=trouve(i)+1;
        end 
        taille(i)=taille(i)+length(myJ2);
    % v�rification du J 
    % display(realH);
    % display(greedyH);
    % display(myH);
    % % display(fpgH);
    % 
    % display(sort(realJ));
    % display(sort(greedyJ))
    % %display(sort(fpgJ));
    % display(sort(myJ));
    % 
    % display(norm(X-D*greedyH)/norm(X));
    % % display(norm(X-D*fpgH)/norm(X));
        % display(norm(X-D*myH)/norm(X));
    end 
    
    
%     succesG(i)=succesG(i)/itt;
%     succesE(i)=succesE(i)/itt;
%     succesE2(i)=succesE2(i)/itt;
       trouve(i)=trouve(i)/itt;
       taille(i)=taille(i)/itt;
end 
  %% Plots and comparison

% % Evolution du nombre de succ�s en fct du niveau de bruit
% figure
% plot(nl,succesG,nl,succesE,nl,succesE2)
% legend('greedy','exact','matopie')
% title('Evolution du taux de pr�diction de J en fonction du niveau de bruit')
% xlabel('pourcentage de bruit')
% ylabel('taux de succ�s')
% TODO% figure
figure
plot(nl,trouve)
legend('matopie')
title('taux ou J contient le realJ en fct du bruit pour s=20')
xlabel('pourcentage de bruit')
ylabel('taux ')
figure
plot(nl,taille)
legend('matopie')
title('taille du J en fct du bruit pour s=20')
xlabel('pourcentage de bruit')
ylabel('taille ')

end 
if choix==2
 %% Initialisation
itt=10;
s=100;
n=200;
spas=100;
npas=0;
ntest=4;
tempsG=zeros(ntest,1); % greedy
tempsE=zeros(ntest,1); % exacte 
tempsE2=zeros(ntest,1); % exacte 
ls=linspace(s,s*ntest,ntest)
%D = datalib(:,randperm(nbcol_usgs-5, s)+5);
illcondA=0;
for i=1:ntest
    
    disp(s);
    for j=1:itt
        % Build small dictionary by taking s random column from USGS and avoid the
         % first 5 elements (werid spectra)
       D = datalib(:,randperm(nbcol_usgs-5, s)+5);
        if illcondA == 1
        [u,~,v] = svd(D);
        d = diag( logspace(-6,0,s) );
         D = u(:,1:s)*d(1:s,1:s)*v(:,1:s)';
        end
     % Generate synthetic data
     realH = zeros(s, n);
     realJ = randperm(s, r);
     realH(realJ,:) = rand(r, n);
     X = D*realH;

    % Add noise
     noise = randn(m,n);  
     Xn = X + nl(1) * noise/norm(noise,'fro') * norm(X,'fro');

    %% Run algorithms

    % TODO NN Fix this method
    % Run projected gradient with l_{1,inf} projection
    % options = [];
    % % First find one good lambda automatically (may be too slow on big data)
    % [~, lambda] = findlambda_fpgm_nssc(Xn, D, r, options);
    % %lambda = 0; % You can also predefine lambda yourself
    % % Then run the algorithm with the lambda found
    % [fpgH,~,~] = nnls_FPGM_nssc(Xn, D, lambda, options);
    % fpgJ = nonzerorows(fpgH);

    % Run NNS_greedy (greedy algorithm)
    tic;
    greedyH = NNS_greedy(D,Xn,r);
    greedyJ = nonzerorows(greedyH);
    tG=toc;
    % Run our algorithm
    % TODO Alexandra
%         tic;
%         [myJ, myW, myH] = myalgo(Xn, D, r);
%         tE=toc;
         tic;
        [ myH2,myJ2] = matopie3(Xn, D, r);
        tE2=toc;
%     fprintf('Le temps d execution methode exacte %d \n',te);

    %% Plots and comparison
   
        tempsG(i)=tempsG(i)+tG;
   
  
%         tempsE(i)=tempsE(i)+tE;  
        tempsE2(i)=tempsE2(i)+tE2; 
  	 
    % v�rification du J 
    % display(realH);
    % display(greedyH);
    % display(myH);
    % % display(fpgH);
    % 
    % display(sort(realJ));
    % display(sort(greedyJ))
    % %display(sort(fpgJ));
    % display(sort(myJ));
    % 
    % display(norm(X-D*greedyH)/norm(X));
    % % display(norm(X-D*fpgH)/norm(X));
        % display(norm(X-D*myH)/norm(X));
    end 
    
    
    tempsG(i)=tempsG(i)/itt;
%     tempsE(i)=tempsE(i)/itt;
    tempsE2(i)=tempsE2(i)/itt;
    s=s+spas;
    n=s*2;
    
end 
  %% Plots and comparison

% Evolution du nombre de succ�s en fct du niveau de bruit
figure
plot(ls,tempsG,ls,tempsE2)
legend('greedy','matopie')
title('Evolution du temps en fonction de s')
xlabel('s')
ylabel('temps')   
end 
if choix==3
 %% Initialisation
s=200;
n=400;
r=4;
itt=50;
tE=0;
tt=zeros(itt,1);
w=s*ones(itt,1);
%D = datalib(:,randperm(nbcol_usgs-5, s)+5);

illcondA=1;
for i=1:itt
    
   disp(i)
  
   
        % Build small dictionary by taking s random column from USGS and avoid the
         % first 5 elements (werid spectra)
       D = datalib(:,randperm(nbcol_usgs-5, s)+5);
       if illcondA == 1
        [u,~,v] = svd(D);
        d = diag( logspace(-6,0,s) );
         D = u(:,1:s)*d(1:s,1:s)*v(:,1:s)';
        end
       
     % Generate synthetic data
     realH = zeros(s, n);
     realJ = randperm(s, r);
     realH(realJ,:) = rand(r, n);
     X = D*realH;

    % Add noise
     noise = randn(m,n);  
     Xn = X + nl(1) * noise/norm(noise,'fro') * norm(X,'fro');

    %% Run algorithms

    % TODO NN Fix this method
    % Run projected gradient with l_{1,inf} projection
    % options = [];
    % % First find one good lambda automatically (may be too slow on big data)
    % [~, lambda] = findlambda_fpgm_nssc(Xn, D, r, options);
    % %lambda = 0; % You can also predefine lambda yourself
    % % Then run the algorithm with the lambda found
    % [fpgH,~,~] = nnls_FPGM_nssc(Xn, D, lambda, options);
    % fpgJ = nonzerorows(fpgH);

    % Run NNS_greedy (greedy algorithm)
%     tic;
%     greedyH = NNS_greedy(D,X,r);
%     greedyJ = nonzerorows(greedyH);
%     tG=toc;
    % Run our algorithm
    % TODO Alexandra
       
         tic;
       [myH,myJ]=matopie3(Xn,D,r);
        tE=toc;
        disp(tE);
        tt(i)=tE
%     fprintf('Le temps d execution methode exacte %d \n',te);

    %% Plots and comparison
   
        
  	 
    % v�rification du J 
    % display(realH);
    % display(greedyH);
    % display(myH);
    % % display(fpgH);
    % 
    % display(sort(realJ));
    % display(sort(greedyJ))
    % %display(sort(fpgJ));
    % display(sort(myJ));
    % 
    % display(norm(X-D*greedyH)/norm(X));
    % % display(norm(X-D*fpgH)/norm(X));
        % display(norm(X-D*myH)/norm(X));
   
    
   i=i+1;

  %% Plots and comparison
end 
moy=sum(tt)/itt;
figure
plot(w,tt,w(1),moy,'x')

title('Evolution du temps en fonction de s')
xlabel('s')
ylabel('temps')  
end 
if choix==4
succesG=zeros(nb,1); % greedy
succesE=zeros(nb,1); % exacte 
succesE2=zeros(nb,1); % exacte 
%D = datalib(:,randperm(nbcol_usgs-5, s)+5);
  
illcondA=1;
for i=1:nb
    
    disp(i);
    for j=1:itt
        % Build small dictionary by taking s random column from USGS and avoid the
         % first 5 elements (werid spectra)
       D = datalib(:,randperm(nbcol_usgs-5, s)+5);
       % rendre D mal conditionn�
       if illcondA == 1
        [u,~,v] = svd(D);
        d = diag( logspace(-6,0,s) );
         D = u(:,1:s)*d(1:s,1:s)*v(:,1:s)';
        end
     % Generate synthetic data
     realH = zeros(s, n);
     realJ = randperm(s, r);
     realH(realJ,:) = rand(r, n);
     X = D*realH;

    % Add noise
     noise = randn(m,n);  
     Xn = X + nl(i) * noise/norm(noise,'fro') * norm(X,'fro');

    %% Run algorithms

    % TODO NN Fix this method
    % Run projected gradient with l_{1,inf} projection
    % options = [];
    % % First find one good lambda automatically (may be too slow on big data)
    % [~, lambda] = findlambda_fpgm_nssc(Xn, D, r, options);
    % %lambda = 0; % You can also predefine lambda yourself
    % % Then run the algorithm with the lambda found
    % [fpgH,~,~] = nnls_FPGM_nssc(Xn, D, lambda, options);
    % fpgJ = nonzerorows(fpgH);

    % Run NNS_greedy (greedy algorithm)
    greedyH = NNS_greedy(D,X,r);
    greedyJ = nonzerorows(greedyH);

    % Run our algorithm
    % TODO Alexandra
%         tic;
%         [myJ, myW, myH] = myalgo(Xn, D, r);
%         te=toc;
        tic;
        [myH2,myJ2]=matopie(X,D,r);
        te=toc;
%     fprintf('Le temps d execution methode exacte %d \n',te);


    %% Plots and comparison
     %if sum(realJ==greedyJ)==length(realJ)
     if isequal(sort(realJ), sort(greedyJ))
        succesG(i)=succesG(i)+1;
     end 
     %if sum(realJ==myJ)==length(realJ)
%      if isequal(sort(realJ), sort(myJ))
%         succesE(i)=succesE(i)+1;   
%      end 
      if isequal(sort(realJ), sort(myJ2))
        succesE2(i)=succesE2(i)+1;   
  	 end 
    % v�rification du J 
    % display(realH);
    % display(greedyH);
    % display(myH);
    % % display(fpgH);
    % 
    % display(sort(realJ));
    % display(sort(greedyJ))
    % %display(sort(fpgJ));
    % display(sort(myJ));
    % 
    % display(norm(X-D*greedyH)/norm(X));
    % % display(norm(X-D*fpgH)/norm(X));
        % display(norm(X-D*myH)/norm(X));
    end 
    
    
    succesG(i)=succesG(i)/itt;
%     succesE(i)=succesE(i)/itt;
    succesE2(i)=succesE2(i)/itt;
end 
  %% Plots and comparison

% Evolution du nombre de succ�s en fct du niveau de bruit
figure
plot(nl,succesG,nl,succesE2)
legend('greedy','matopie')
title('Evolution du taux de pr�diction de J en fonction du niveau de bruit')
xlabel('pourcentage de bruit')
ylabel('taux de succ�s')    
    
end
