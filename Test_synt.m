clear variables; clc;
addpath('./util');
addpath('./heuristic_nssc');
addpath('./heuristic_nssc/prox-l1oo');
addpath('./hierclust2nmf_v2');
addpath 'C:\Users\alexi\Matlab\ancien code'
addpath 'Acc_MU_HALS_PG'
addpath 'web'




%% Test heuristic and hybrid heuristic-exact on synthetic data 

%% Parameters


s = 100;   % number of columns of the dictionary D, to extract from USGS
rmax=30; % number of columns extract by heuristic for hybrid 
r = 5;    % number of columns of W, to select from the dictionary
tl =60*5; % limit time for the exact solving  
n = 100;  % number of data points (numer of columns of input X)
option=0; % NN constraint 
itt=10;% ittération by noise level  

nb=10; % number of noise level between 10^-10 et 1

nl = logspace(-3,-0.5,nb); % noise level to add to generated data

%% Data generation
% Select a subset of the USGS dataset to build a dictionary D
load("data/USGS_Library.mat");
[m, nbcol_usgs] = size(datalib);


 

    
    %% Initialisation 
    rS=zeros(nb,1); % greedy
    rE=zeros(nb,1); % exacte 
    rE2=zeros(nb,1); % exacte 
    rO=zeros(nb,1);
    for i=1:nb
     for j=1:itt
        disp(i);
        % Build small dictionary by taking s random column from USGS and avoid the
         % first 5 elements (werid spectra)
       D = datalib(:,randperm(nbcol_usgs-5, s)+5);
       
        % normalisation des colonnes de D 
        for j=1:s
            D(:,j)=D(:,j)./norm(D(:,j));
        end 
     % Generate synthetic data
     realH = zeros(s, n);
     realJ = randperm(s, r);
     realH(realJ,:) = randn(r, n);
     X = D*realH;

    % Add noise
     noise = randn(m,n);  
     Xn = X + nl(i) * noise/norm(noise,'fro') * norm(X,'fro');

    %% Run algorithms
    % NNS-SP algorithm
    [X_nnssp,J_nnssp]=NNS_greedy(D,Xn,r,'alg','sp','nonneg','off','simult','on');
    
     rS(i)=rS(i)+(10-length(setdiff(J_nnssp,realJ)))/10;
     % NNS-SP algorithm + exact 
    [X_nnssp,Jnn]=NNS_greedy(D,Xn,rmax(1),'alg','sp','nonneg','off','simult','on');
     [J,H,results] = exact(Xn, D(:,Jnn), r,tl,option);
     J1=Jnn(J);
      rE(i)=rE(i)+(10-length(setdiff(J1,realJ)))/10;
    % NNS-SP algorithm
    [X_nnssp,J_nnssp]=NNS_greedy(D,Xn,r,'alg','omp','nonneg','off','simult','on');
    
     rO(i)=rO(i)+(10-length(setdiff(J_nnssp,realJ)))/10;
     % NNS-SP algorithm + exact 
    [X_nnssp,Jnn]=NNS_greedy(D,Xn,rmax(1),'alg','omp','nonneg','off','simult','on');
     [J,H,results] = exact(Xn, D(:,Jnn), r,tl,option);
     J1=Jnn(J);
      rE2(i)=rE2(i)+(10-length(setdiff(J1,realJ)))/10;
   
        end 
    rE(i)=rE(i)/itt;
    rS(i)=rS(i)/itt;
    rO(i)=rO(i)/itt; 
        rE2(i)=rE2(i)/itt;
    end 
  %% Plots and comparison

    % % Evolution de l'erreur en fct du niveau de bruit
   figure
    plot(db(nl),rS.*100,db(nl),rE.*100,db(nl),rO.*100,db(nl),rE2.*100)
    legend('S-SP','S-SP+exact','S-OMP','S-OMP+exact')
    xlabel('Noise level (dB)')
    ylabel('Percentage of correct columns (%)')
    ytickformat('percentage')

