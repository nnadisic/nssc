% Algorithm to solve Nonnegative Simultaneous Sparse Coding 
% with the non-negativity constraint optional
%
% given a matrix X (m x n) (>=0), a dictionary D (m x s) and 
% an integer r, find a matrix H (r x n)(>=0) and an index set J 
% with r elements such that D(:,J) H is close to X. 
% 
% min_{J,H(>=0)} || X - D(:,J) H ||_F^2 such that J in {1,2,...,s}, |K|=r. 
% 
% 
% *** Input ****
% X       :  (nonnegative) matrix to approximate
% D       : dictionary 
% r       : number of column to select in the dictionary
% tl      : limit time for gurobi solver
% option  : non-negativity constraint 
%         = false : desactivate 
%         = true  :activate 
%
% *** Output ****
% (J,H)   :  X approx D(:,J)H, (with H>= 0), |J| = r 
% results     : results of gurobi solver 



function [J,H,results] = exact(X, D, r,tl,option)

[~, n] = size(X);
[m, s] = size(D);
H=zeros(s,n);
J=zeros(1,r);

% M
M=100;
%% model building 
% objectif matrix 
Q=sparse(s*n+s,s*n+s);
DtD=sparse(D'*D);
XD=-2*X'*D;
qt=reshape(XD',[],1);
qt=qt';
qt=[qt,zeros(1,s)];
Q(1:n*s,1:n*s)=diagblock(DtD,n);
 
model.Q = Q;
model.obj = full(qt');

%variables 
model.vtype(1:n*s)='C'; %H
model.vtype(n*s+1:n*s+s)='B';%y

% constraints matrix

    A=sparse(2*s*n+1,n*s+s);
    l=zeros(2*s*n+1,1);
    
% constraint 1 
 A(1:n*s,n*s+1:n*s+s)=repmat(spdiags(-M*ones(s,1),0,s,s),n,1);
 A(1:n*s,1:n*s)=spdiags(ones(n*s,1),0,n*s,n*s);
 model.ub(1:n*s)=M;
 model.ub(n*s:n*s+s)=1;
 %constraint 2
 A(n*s+1,n*s+1:n*s+s)=ones(1,s);
 l(n*s+1)=r;
%  constraint 3 
if option
    A(n*s+2:2*n*s+1,1:n*s)=spdiags(-ones(n*s,1),0,n*s,n*s); %% case NN
else 
    A(n*s+2:2*n*s+1,n*s+1:n*s+s)=repmat(spdiags(-M*ones(s,1),0,s,s),n,1);
    A(n*s+2:2*n*s+1,1:n*s)=spdiags(-ones(n*s,1),0,n*s,n*s);
end 
if ~option 
    
    model.lb(1:n*s)= -M;
    model.lb(n*s:n*s+s)= 0;
end 
model.A = A;
model.rhs = l;
model.sense(1:n*s) = '<';
model.sense(n*s+1) = '<';
model.sense(s*n+2:2*s*n+1) = '<';

%% Gurobi 
% gurobi settings  
params.outputflag = 1 ; % console display
params.timelimit=tl;   % limit time 
params.NonConvex=2; % due to calculation noise
disp('model done')
gurobi_write(model, 'mqp.lp');  
results  = gurobi(model,params);

%retrieval of results
for j=0:n-1
    H(:,j+1)=results.x(1+j*s:s+j*s);
end 

for i=1:r
    J = nonzerorows(H);
end 

end