function [val] = norml1inf(X)
%NORML1INF Compute the mixed norm l_{1,inf} of the input matrix X, defined
% as Sum_i || X(i,:) ||_inf
val = sum(max(X, [], 2));
end

